import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { LocalStorageService } from '../local-storage/local-storage.service';

import { AUTH_KEY, AuthActionTypes } from './store/';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions<Action>,
    private localStorageService: LocalStorageService,
    private router: Router
  ) { }

  /*   @Effect()
    check$: Observable<Action> = this.actions$.pipe(
      ofType(AuthActionTypes.CHECK),
      tap(action => this.localStorageService.authTokenCheck())
    );
   */
  @Effect({ dispatch: false })
  login(): Observable<Action> {
    return this.actions$
      .ofType(AuthActionTypes.LOGIN)
      .pipe(
        tap(action => {
          console.log(this.localStorageService.authTokenCheck());
          if (this.localStorageService.authTokenCheck()) {
            this.localStorageService.setItem('_a_status', { isAuthenticated: true });
          } else {
            console.error(new Error('Authorization error'));
          }
        }
        )
      );
  }

  @Effect({ dispatch: false })
  logout(): Observable<Action> {
    return this.actions$.ofType(AuthActionTypes.LOGOUT).pipe(
      tap(action => {

        this.localStorageService.setItem('_a_status', { isAuthenticated: false });
      })
    );
  }
}
