import { Action } from '@ngrx/store';

export enum AuthActionTypes {
    LOGIN = '[Auth] Login',
    LOGOUT = '[Auth] Logout',
    CHECK = '[Auth] Check',

}

export const AUTH_KEY = 'AUTH';


export class ActionAuthLogin implements Action {
    readonly type = AuthActionTypes.LOGIN;
}

export class ActionAuthLogout implements Action {
    readonly type = AuthActionTypes.LOGOUT;
}

export class ActionAuthCheck implements Action {
    readonly type = AuthActionTypes.CHECK;
    constructor(public payload: boolean) {
    }
}



export type AuthActions = ActionAuthLogin | ActionAuthLogout | ActionAuthCheck;

