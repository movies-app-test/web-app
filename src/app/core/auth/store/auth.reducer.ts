import { AuthActions, AuthActionTypes } from './auth.actions';

//
export interface AuthState {
    isAuthenticated: boolean;
}

export const initialState: AuthState = {
    isAuthenticated: false,
};


export const selectorAuth = state => state.auth;

//
export function authReducer(
    state: AuthState = initialState,
    action: AuthActions
): AuthState {
    switch (action.type) {
        case AuthActionTypes.LOGIN:
            return { ...state, isAuthenticated: true };

        case AuthActionTypes.LOGOUT:
            localStorage.clear();
            return { ...state, isAuthenticated: false };

        case AuthActionTypes.CHECK:
            return { ...state, isAuthenticated: action.payload };

        default:
            return state;
    }
}
