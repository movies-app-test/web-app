import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared';
import { CoreModule } from '@app/core';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SettingsModule } from './settings';
import { StaticModule } from './static';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EffectsModule } from '@ngrx/effects';

import { MatGridListModule, MatTooltipModule } from '@angular/material';

import { MoviesModule } from './movies/movies.module';
import { MoviesEffects } from './movies/store/movies.effects';

import { MatIconModule } from '@angular/material/icon';



@NgModule({
  imports: [
    // angular
    BrowserAnimationsModule,
    BrowserModule, FormsModule,
    ReactiveFormsModule,


    // material elements
    MatInputModule,
    MatDialogModule,
    MatExpansionModule,
    MatCardModule,
    MatListModule,
    MatGridListModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,

    // core & shared
    CoreModule,
    SharedModule,

    // features
    StaticModule,
    SettingsModule,


    MoviesModule,

    // app
    AppRoutingModule,
    EffectsModule.forRoot([MoviesEffects]),

    /*     StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      // logOnly: environment.production, // Restrict extension to log-only mode
    }), */
  ],
  declarations: [AppComponent],
  providers: [],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
