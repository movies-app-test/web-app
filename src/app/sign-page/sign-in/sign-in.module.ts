import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { signInreducer } from './store/sign-in.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('signIn', signInreducer),
  ],
  declarations: []
})
export class SignInModule { }
