import { Action } from '@ngrx/store';
import { SignInUser } from '../model/sign-in.interface';



export enum SignInUserActionsTypes {
    API_SIGN_IN = '[SignInUser] ApiSignIn',
    API_SIGN_IN_SUCCESS = '[SignInUser] ApiSignInSuccess',
    API_SIGN_IN_FAILURE = '[SignInUser] ApiSignInFailure'
}



export class ApiSignIn implements Action {
    readonly type = SignInUserActionsTypes.API_SIGN_IN;

    constructor(public payload: SignInUser) { }
}

export class ApiSignInSuccess implements Action {
    readonly type = SignInUserActionsTypes.API_SIGN_IN_SUCCESS;

    constructor(public payload: SignInUser) { }
}

export class ApiSignInFailure implements Action {
    readonly type = SignInUserActionsTypes.API_SIGN_IN_FAILURE;

    constructor(public payload: string) { }
}

export type SignInUserActions =
    ApiSignIn | ApiSignInSuccess | ApiSignInFailure;
