import { SignInUserActions, SignInUserActionsTypes } from './sign-in.actions';
import { SignInUser } from '../model/sign-in.interface';



export interface SignInUserState {
    user: SignInUser;
    isSignedIn: boolean;
    error: string;
}

export const initialState: SignInUserState = {
    user: {
        email: '',
        password: ''
    },
    isSignedIn: false,
    error: '',
};

export const signInSelector = state => state.signIn;

export function signInreducer(state = initialState, action: SignInUserActions): SignInUserState {
    switch (action.type) {
        case SignInUserActionsTypes.API_SIGN_IN: {
            return {
                // return new class state
                ...state,
                user: action.payload,
                isSignedIn: false,
            };
        }
        case SignInUserActionsTypes.API_SIGN_IN_SUCCESS: {
            return {
                // return new class state
                ...state,
                user: action.payload,
                isSignedIn: true,
            };
        }
        case SignInUserActionsTypes.API_SIGN_IN_FAILURE: {
            return {
                // return new class state
                ...state,
                isSignedIn: false,
                error: action.payload
            };
        }

        default: {
            return state;
        }
    }
}
