import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { SignInUserActionsTypes } from './sign-in.actions';
import { ActionAuthLogin } from '../../../core/auth/store/auth.actions';
import { Router } from '@angular/router';
import { api } from '../../../../environments/environment';


@Injectable()
export class SignInEffects {
    @Effect()
    signIn$: Observable<Action> = this.actions$.pipe(
        ofType(SignInUserActionsTypes.API_SIGN_IN),
        mergeMap(action => {
            const header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                .set('Access-Control-Allow-Methods', 'POST')
                .set('Content-type', 'application/json');

            return this.http.post(`${api}/auth/sign-in`, JSON.stringify(action['payload']), {
                headers: header,
                observe: 'response',
            }).pipe(
                // If successful, dispatch success action with result
                map(data => {
                    localStorage.setItem('a_token', data.headers.get('authorization'));
                    this.router.navigate(['shops']);
                    this.store.dispatch(new ActionAuthLogin());
                    return ({ type: SignInUserActionsTypes.API_SIGN_IN_SUCCESS, payload: data.body });
                }
                ),
                // If request fails, dispatch failed action
                catchError((error) => of({ type: SignInUserActionsTypes.API_SIGN_IN_FAILURE, payload: error }))
            );
        }
        )
    );
    constructor(private http: HttpClient,
        private actions$: Actions,
        private store: Store<any>,
        private router: Router) { }
}
