import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, ReactiveFormsModule } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { SignInUser } from './model/sign-in.interface';
import { Store } from '@ngrx/store';
import { ApiSignIn } from './store/sign-in.actions';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'anms-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(public store: Store<any>) { }

  user: SignInUser = {
    email: '',
    password: '',
  };

  valid: boolean;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(18),
  ]);

  matcher = new MyErrorStateMatcher();

  ngOnInit() {
    // this.store.subscribe(store => { return this.user = store.signIn.user; });
  }

  onFormFilled() {
    if (this.passwordFormControl.status === 'VALID' && this.emailFormControl.status === 'VALID') {
      if (!this.passwordFormControl.hasError('required') && !this.emailFormControl.hasError('required')) {
        // --> Action
        this.user.email = this.emailFormControl.value;
        this.user.password = this.passwordFormControl.value;
        this.store.dispatch(new ApiSignIn({ ...this.user }));
      }
    }
  }

}
