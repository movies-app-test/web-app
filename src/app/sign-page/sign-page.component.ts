import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'anms-sign-page',
  templateUrl: './sign-page.component.html',
  styleUrls: ['./sign-page.component.css']
})
export class SignPageComponent implements OnInit {

  constructor() {
  }
  state: boolean;

  ngOnInit() {

  }

  onSignIn() {
    this.state = true;
    // Action -->
  }

  onSignUp() {
    this.state = false;
    // Action -->
  }

}
