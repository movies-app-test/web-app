import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { SignUpUser } from './models/sign-up.interface';
import { ApiSignUp } from './store/sign-up.actions';
import { Store } from '@ngrx/store';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'anms-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(public store: Store<any>) { }

  user: SignUpUser = {
    email: '',
    displayName: '',
    password: ''
  };

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(18),
    Validators.pattern('[a-zA-Z0-9 ]*')
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
    Validators.maxLength(18),
    Validators.pattern('[a-zA-Z0-9 ]*')
  ]);

  passwordFormControlRe = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
    Validators.maxLength(18),
    Validators.pattern('[a-zA-Z0-9 ]*')
  ]);

  matcher = new MyErrorStateMatcher();

  ngOnInit() { }

  onSignUp() {
    this.user.email = this.emailFormControl.value;
    this.user.password = this.passwordFormControl.value;
    this.user.displayName = this.nameFormControl.value;
    this.store.dispatch(new ApiSignUp({ ...this.user }));
  }

}
