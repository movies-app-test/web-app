import { SignUpUser } from '../models/sign-up.interface';
import { Action } from '@ngrx/store';

export enum SignUpActionsTypes {
    API_SIGNUP = '[SignUpUser] ApiSignUp',
    API_SIGNUP_SUCCESS = '[SignUpUser] ApiSignUpSuccess',
    API_SIGNUP_FAILURE = '[SignUpUser] ApiSignUpFailure',
}


export class ApiSignUp implements Action {
    readonly type = SignUpActionsTypes.API_SIGNUP;

    constructor(public payload: SignUpUser) { }
}

export class ApiSignUpSuccess implements Action {
    readonly type = SignUpActionsTypes.API_SIGNUP_SUCCESS;

    constructor(public payload: SignUpUser) { }
}

export class ApiSignUpFailure implements Action {
    readonly type = SignUpActionsTypes.API_SIGNUP_FAILURE;

    constructor(public payload: string) { }
}

export type SignUpActions =
    ApiSignUp | ApiSignUpSuccess | ApiSignUpFailure;

