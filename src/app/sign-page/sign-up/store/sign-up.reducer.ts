import { SignUpUser } from '../models/sign-up.interface';
import { SignUpActions, SignUpActionsTypes } from './sign-up.actions';

export interface SignUpState {
    user: SignUpUser;
    isSignedUp: boolean;
    error: string;
}

const initialState: SignUpState = {
    user: {
        displayName: '',
        email: '',
        password: ''
    },
    isSignedUp: false,
    error: ''
};

export const signUpSelector = state => state.signUp;

export function signUpReducer(state = initialState, action: SignUpActions): SignUpState {
    switch (action.type) {
        case SignUpActionsTypes.API_SIGNUP: {
            return {
                // return new class state
                ...state,
                user: action.payload,
                isSignedUp: false,
            };
        }
        case SignUpActionsTypes.API_SIGNUP_FAILURE: {
            return {
                // return new class state
                ...state,
                error: 'Error',
                isSignedUp: false,
            };
        }
        case SignUpActionsTypes.API_SIGNUP_SUCCESS: {
            return {
                // return new class state
                ...state,
                user: action.payload,
                isSignedUp: true,
            };
        }

        default: {
            return state;
        }
    }
}
