import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { SignUpActionsTypes } from './sign-up.actions';
import { Router } from '@angular/router';
import { api } from '../../../../environments/environment';
import { ActionAuthLogin } from '../../../core/auth/store/auth.actions';


@Injectable()
export class SignUpEffects {
    // Listen for the 'LOGIN' action
    @Effect()
    signUp$: Observable<Action> = this.actions$.pipe(
        ofType(SignUpActionsTypes.API_SIGNUP),
        mergeMap(action => {
            const header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                .set('Access-Control-Allow-Methods', 'POST')
                .set('Content-type', 'application/json');
            return this.http.post(`${api}/users/`, JSON.stringify(action['payload']), {
                headers: header,
                observe: 'response',
            }).pipe(
                // If successful, dispatch success action with result
                map(data => {
                    this.router.navigate(['shops']);
                    localStorage.setItem('a_token', data.headers.get('authorization'));
                    this.store.dispatch(new ActionAuthLogin());
                    return ({ type: SignUpActionsTypes.API_SIGNUP_SUCCESS, payload: data.body });
                }),
                // If request fails, dispatch failed action
                catchError(() => of({ type: SignUpActionsTypes.API_SIGNUP_FAILURE }))
            );
        }
        )
    );

    constructor(private http: HttpClient, private actions$: Actions, private router: Router, private store: Store<any>) { }
}
