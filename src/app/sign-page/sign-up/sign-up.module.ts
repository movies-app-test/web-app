import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { signUpReducer } from './store/sign-up.reducer';



@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('signUp', signUpReducer),
  ],
  declarations: []
})
export class SignUpModule { }
