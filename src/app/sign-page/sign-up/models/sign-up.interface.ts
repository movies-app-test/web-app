export interface SignUpUser {
    email: string;
    password: string;
    displayName: string;
}
