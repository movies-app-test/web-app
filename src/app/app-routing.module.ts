import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesPageComponent } from './movies/movies-page/movies-page.component';

const routes: Routes = [
  {
    path: 'movies',
    component: MoviesPageComponent,
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'movies'
  },
];

@NgModule({
  // useHash supports github.io demo page, remove in your app
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
