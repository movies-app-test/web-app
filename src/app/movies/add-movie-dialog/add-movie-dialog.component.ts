import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';




export interface AddMovieDialogData {
  title: string;
}
@Component({
  selector: 'anms-add-movie-dialog',
  templateUrl: './add-movie-dialog.component.html',
  styleUrls: ['./add-movie-dialog.component.scss']
})


export class AddMovieDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddMovieDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AddMovieDialogData) { }

  titleInput: FormControl;

  ngOnInit() {
    this.titleInput = new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20)
    ]);

  }

  onCreateClick() {
    this.dialogRef.close(this.titleInput.value)
  }


}
