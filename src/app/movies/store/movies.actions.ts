import { Action } from '@ngrx/store';


import { MovieRateDTO, MovieAddDTO, Movie } from './movies.interfaces';

export interface CustomAction {
    type: string;
    payload?: any;
}


export enum MoviesActionTypes {
    LOAD = '[Movies] Load',
    LOAD_SUCCESS = '[Movies] LoadSuccess',
    LOAD_FAILURE = '[Movies] LoadFailure',

    ADD = '[Movies] Add',
    ADD_SUCCESS = '[Movies] AddSuccess',
    ADD_FAILURE = '[Movies] AddFailure',

    REMOVE = '[Movies] Remove',
    REMOVE_SUCCESS = '[Movies] RemoveSuccess',
    REMOVE_FAILURE = '[Movies] RemoveFailure',

    RATE = '[Movies] Rate',
    RATE_SUCCESS = '[Movies] RateSuccess',
    RATE_FAILURE = '[Movies] RateFailure',

    LOAD_TOTAL_RATING = '[Movies] LoadTotalRating',
    LOAD_TOTAL_RATING_SUCCESS = '[Movies] LoadTotalRatingSuccess',
    LOAD_TOTAL_RATING_FAILURE = '[Movies] LoadTotalRatingFailure',

}

export class ActionMoviesLoad implements CustomAction {
    readonly type = MoviesActionTypes.LOAD;
}

export class ActionMoviesLoadSuccess implements CustomAction {
    readonly type = MoviesActionTypes.LOAD_SUCCESS;
    constructor(public payload: Movie[]) { }
}

export class ActionMoviesLoadFailure implements CustomAction {
    readonly type = MoviesActionTypes.LOAD_FAILURE;
}

export class ActionMoviesAdd implements CustomAction {
    readonly type = MoviesActionTypes.ADD;
    constructor(public payload: MovieAddDTO) { }
}

export class ActionMoviesAddSuccess implements CustomAction {
    readonly type = MoviesActionTypes.ADD_SUCCESS;
    constructor(public payload: Movie[]) { }
}

export class ActionMoviesAddFailure implements CustomAction {
    readonly type = MoviesActionTypes.ADD_FAILURE;
}

export class ActionMoviesRemove implements CustomAction {
    readonly type = MoviesActionTypes.REMOVE;
    constructor(public payload: string) { }
}

export class ActionMoviesRemoveSuccess implements CustomAction {
    readonly type = MoviesActionTypes.REMOVE_SUCCESS;
    constructor(public payload: Movie[]) { }
}

export class ActionMoviesRemoveFailure implements CustomAction {
    readonly type = MoviesActionTypes.REMOVE_FAILURE;
}
export class ActionMoviesRate implements CustomAction {
    readonly type = MoviesActionTypes.RATE;
    constructor(public payload: MovieRateDTO) { }
}
export class ActionMoviesRateSuccess implements CustomAction {
    readonly type = MoviesActionTypes.RATE_SUCCESS;
    constructor(public payload: Movie[]) { }
}

export class ActionMoviesRateFailure implements CustomAction {
    readonly type = MoviesActionTypes.RATE_FAILURE;
}

export class ActionMoviesLoadTotalRating implements CustomAction {
    readonly type = MoviesActionTypes.LOAD_TOTAL_RATING;
}

export class ActionMoviesLoadTotalRatingSuccess implements CustomAction {
    readonly type = MoviesActionTypes.LOAD_TOTAL_RATING_SUCCESS;
    constructor(public payload: number) { }
}

export class ActionMoviesLoadTotalRatingFailure implements CustomAction {
    readonly type = MoviesActionTypes.LOAD_TOTAL_RATING_FAILURE;
}


export type MoviesActions =
    | ActionMoviesLoad
    | ActionMoviesLoadSuccess
    | ActionMoviesLoadFailure
    | ActionMoviesAdd
    | ActionMoviesAddSuccess
    | ActionMoviesAddFailure
    | ActionMoviesRemove
    | ActionMoviesRemoveSuccess
    | ActionMoviesRemoveFailure
    | ActionMoviesRate
    | ActionMoviesRateSuccess
    | ActionMoviesRateFailure
    | ActionMoviesLoadTotalRating
    | ActionMoviesLoadTotalRatingSuccess
    | ActionMoviesLoadTotalRatingFailure;


