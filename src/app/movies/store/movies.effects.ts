import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
    MoviesActionTypes, CustomAction, ActionMoviesLoad,
    ActionMoviesLoadTotalRating
} from './movies.actions';
import { mergeMap, catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { api } from '@env/environment';

@Injectable()
export class MoviesEffects {
    constructor(
        private http: HttpClient,
        private actions$: Actions,
        private moviesStore: Store<any>
    ) { }


    @Effect() loadMovies$: Observable<Action> = this.actions$.pipe(
        ofType(MoviesActionTypes.LOAD),
        mergeMap(
            action => {
                const header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                    .set('Access-Control-Allow-Methods', 'GET')
                    .set('Content-type', 'application/json');


                return this.http.get(`${api}/movies/`, {
                    headers: header,
                    responseType: 'json'
                }
                ).pipe(
                    map(data => {
                        this.moviesStore.dispatch(new ActionMoviesLoadTotalRating())
                        return ({ type: MoviesActionTypes.LOAD_SUCCESS, payload: data });
                    }),

                    catchError(error => of({ type: MoviesActionTypes.LOAD_FAILURE }))
                );

            }

        )
    );


    @Effect() rate$: Observable<CustomAction> = this.actions$.pipe(
        ofType(MoviesActionTypes.RATE),
        mergeMap(
            action => {
                const header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                    .set('Access-Control-Allow-Methods', 'GET')
                    .set('Content-type', 'application/json');


                return this.http.post(`${api}/movies/rate`, JSON.stringify(action['payload']), {
                    headers: header,
                    responseType: 'json'
                }
                ).pipe(
                    map(data => {
                        this.moviesStore.dispatch(new ActionMoviesLoad())
                        this.moviesStore.dispatch(new ActionMoviesLoadTotalRating())
                        return ({ type: MoviesActionTypes.RATE_SUCCESS, payload: data });
                    }),

                    catchError(error => of({ type: MoviesActionTypes.RATE_FAILURE }))
                );

            }

        )
    );


    @Effect() addMovie$: Observable<CustomAction> = this.actions$.pipe(
        ofType(MoviesActionTypes.ADD),
        mergeMap(
            action => {
                const header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                    .set('Access-Control-Allow-Methods', 'GET')
                    .set('Content-type', 'application/json');


                return this.http.post(`${api}/movies`, JSON.stringify(action['payload']), {
                    headers: header,
                    responseType: 'json'
                }
                ).pipe(
                    map(data => {
                        this.moviesStore.dispatch(new ActionMoviesLoad())
                        return ({ type: MoviesActionTypes.ADD_SUCCESS, payload: data });
                    }),

                    catchError(error => of({ type: MoviesActionTypes.ADD_FAILURE }))
                );

            }

        )
    );

    @Effect() removeMovie$: Observable<CustomAction> = this.actions$.pipe(
        ofType(MoviesActionTypes.REMOVE),
        mergeMap(
            action => {
                const header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                    .set('Access-Control-Allow-Methods', 'GET')
                    .set('Content-type', 'application/json');


                return this.http.delete(`${api}/movies/${action['payload']}`, {
                    headers: header,
                    responseType: 'json'
                }
                ).pipe(
                    map(data => {
                        this.moviesStore.dispatch(new ActionMoviesLoad())
                        return ({ type: MoviesActionTypes.REMOVE_SUCCESS, payload: data });
                    }),

                    catchError(error => of({ type: MoviesActionTypes.REMOVE_FAILURE }))
                );

            }

        )
    );

    @Effect() loadTotalRating$: Observable<Action> = this.actions$.pipe(
        ofType(MoviesActionTypes.LOAD_TOTAL_RATING),
        mergeMap(
            action => {
                const header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                    .set('Access-Control-Allow-Methods', 'GET')
                    .set('Content-type', 'application/json');


                return this.http.get(`${api}/movies/rate-average`, {
                    headers: header,
                    responseType: 'json'
                }
                ).pipe(
                    map(data => {
                        return ({ type: MoviesActionTypes.LOAD_TOTAL_RATING_SUCCESS, payload: data });
                    }),

                    catchError(error => of({ type: MoviesActionTypes.LOAD_TOTAL_RATING_FAILURE }))
                );

            }

        )
    );








}
