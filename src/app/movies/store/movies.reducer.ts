import { Action } from '@ngrx/store';
import { Movie } from './movies.interfaces';
import { MoviesActions, MoviesActionTypes } from './movies.actions';


export interface MoviesState {
    movies: Movie[];
    isMoviesLoading: boolean;
    isMoviesLoadSucess: boolean;
    totalRating: number;
    isRatingLoading: boolean;
    isRatingLoadSucess: boolean;

}

export const initialState: MoviesState = {
    movies: [],
    isMoviesLoading: false,
    isMoviesLoadSucess: false,
    totalRating: undefined,
    isRatingLoading: false,
    isRatingLoadSucess: false
};


export const selectMovies = state => state.movies.movies.movies;
export const selectTotalRating = state => state.movies.movies.totalRating;


export function moviesReducer(
    state: MoviesState = initialState,
    action: MoviesActions
): MoviesState {
    switch (action.type) {
        case MoviesActionTypes.LOAD:
            return {
                ...state,
                isMoviesLoading: true,
                isMoviesLoadSucess: false
            };

        case MoviesActionTypes.LOAD_SUCCESS:
            return {
                ...state,
                isMoviesLoading: false,
                isMoviesLoadSucess: true,
                movies: action.payload
            };

        case MoviesActionTypes.LOAD_FAILURE:
            return {
                ...state,
                isMoviesLoading: false,
                isMoviesLoadSucess: false
            };

        case MoviesActionTypes.LOAD_TOTAL_RATING:
            return {
                ...state,
                isRatingLoading: true,
                isRatingLoadSucess: false
            };

        case MoviesActionTypes.LOAD_TOTAL_RATING_SUCCESS:
            return {
                ...state,
                isRatingLoading: false,
                isRatingLoadSucess: true,
                totalRating: action.payload
            };

        case MoviesActionTypes.LOAD_TOTAL_RATING_FAILURE:
            return {
                ...state,
                isRatingLoading: false,
                isRatingLoadSucess: false
            };


        default:
            return state;
    }


}


