/**
 * Movie object interface
 *
 * @export
 * @interface Movie
 */
export interface Movie {
    _id?: string;
    rating?: number;
    title: string;
}



/**
 * Movie add Data Transfer Object
 *
 * @export
 * @interface MovieAddDTO
 */
export interface MovieAddDTO {
    title: string;
}

/**
 * Movie rate Data Transfer Object
 *
 * @export
 * @interface MovieRateDTO
 */
export interface MovieRateDTO {
    _id: string;
    rating: number;
}