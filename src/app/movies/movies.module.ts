import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieComponent } from './movie/movie.component';
import { MoviesPageComponent } from './movies-page/movies-page.component';
import { StoreModule } from '@ngrx/store';
import { moviesReducer } from './store/movies.reducer';
import { StarRatingComponent } from '../star-rating/star-rating.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddMovieDialogComponent } from './add-movie-dialog/add-movie-dialog.component';


import {
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatDialogModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatRadioModule,
  MatRippleModule,
  MatSnackBarModule,
  MatStepperModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import { RatingTotalComponent } from './rating-total/rating-total.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    StoreModule.forFeature('movies', { movies: moviesReducer }),
    MatListModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,




    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatStepperModule,
    MatDialogModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatRadioModule,
    MatRippleModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatTooltipModule,
    ReactiveFormsModule,


  ],
  declarations: [
    MoviesPageComponent,
    MovieComponent,
    StarRatingComponent,
    AddMovieDialogComponent,
    RatingTotalComponent,

  ],
  entryComponents: [

    AddMovieDialogComponent
  ],
  exports: [
  ]
})
export class MoviesModule { }
