import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingTotalComponent } from './rating-total.component';

describe('RatingTotalComponent', () => {
  let component: RatingTotalComponent;
  let fixture: ComponentFixture<RatingTotalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingTotalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingTotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
