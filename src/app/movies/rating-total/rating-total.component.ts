import { Component, Input, ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'anms-rating-total',
  templateUrl: './rating-total.component.html',
  styleUrls: ['./rating-total.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RatingTotalComponent {

  private _rating: string | number = 'n/a';

  @Input()
  set rating(rating: string | number) {

    if (typeof rating === 'number') {
      rating = Math.round(rating * 10) / 10;
    }
    this._rating = rating || 'n/a';
  }

  get rating(): string | number {

    console.log('rating rerenered onPush!');
    return this._rating;
  }

}
