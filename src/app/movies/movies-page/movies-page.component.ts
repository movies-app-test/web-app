import { Component, OnInit, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { Movie } from '../store/movies.interfaces';
import { selectMovies, selectTotalRating } from '../store/movies.reducer';
import { MoviesActions, ActionMoviesLoad, ActionMoviesRate, ActionMoviesAdd, ActionMoviesRemove } from '../store/movies.actions';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { AddMovieDialogComponent } from '../add-movie-dialog/add-movie-dialog.component';



@Component({
  selector: 'anms-movies-page',
  templateUrl: './movies-page.component.html',
  styleUrls: ['./movies-page.component.scss']
})
export class MoviesPageComponent implements OnInit {

  constructor(
    public store: Store<any>,
    public dialog: MatDialog) { }

  movies: Movie[];
  totalRating: number;

  ngOnInit() {
    this.store.subscribe(store => this.movies = selectMovies(store));
    this.store.subscribe(store => this.totalRating = selectTotalRating(store));
    this.store.dispatch(new ActionMoviesLoad());

  }

  onRatingUpdated(rating: number, _id: string): void {
    this.store.dispatch(new ActionMoviesRate({ rating, _id }));
  }

  onDeleteButton(_id: string): void {
    this.store.dispatch(new ActionMoviesRemove(_id));
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddMovieDialogComponent, {
      width: '320px',
      height: '220px',
      data: { title: '' }
    });



    dialogRef.afterClosed().subscribe(title => title && this.store.dispatch(new ActionMoviesAdd({ title })));

  }

}


