
Create an application for showing and managing a list of movies.
Movies can be added and removed from the list.
When a new movie is added, user is required to specify the name.
User can also rank movies in the list – every movie can have a rating between 1 and 5.
Show average rating of all movies under the list.

We expect to see:


Use of ngrx-store, ngrx selectors, async pipe.
Use of dumb and smart components.
Performance optimization: ChangeDetection.OnPush, trackBy.
Use of ngrx-effects to imitate server queries to get a list of movies.
Input validation.